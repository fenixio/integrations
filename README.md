
![Sparc](https://www.sparcstart.com/wp-content/uploads/2017/09/Sparc-Logo-COLOR-335x80.png)

# SparcStart Integrations

This repo contains documentation and samples of integrations of SparcStartplatform with other systems.


## Integrate a Video List Modal Dialog
This functionality provides an integration at browsewr level which allow to open a browser pesudo-modal dialog which 
will contains a list of videos with name tags and other meatadata.
Also each video will show their own playable thumbnail and a checkbox to select them.
A button "Get Selected Videos" calls to a provided callback function that allows the external system do some task with the selected videos ( for example send it by email ) 
[This HTML](VideoModalDialog/sparc.html) file which can be hosted and played on any web server contains samples on how initialize, and open the dialog
Sample is build in plin Javascript so the same functionality should work in any javascript framework.

## Integrate a Video List API
This functionality allows an external system to call to an API endpoint on the SparctStart platform. Please refers to [this README](VideoListAPI/README.md) to get more information about this integration

