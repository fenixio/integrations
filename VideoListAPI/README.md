![Sparc](https://www.sparcstart.com/wp-content/uploads/2017/09/Sparc-Logo-COLOR-335x80.png)
# SparcStart Integrations

## Integrate a Video List API
This functionality allows an external system to call to an API endpoint on the SparctStart platform.

## API Headers
One header with the API Authorization key is mandatory in every call to the SparhStart API. 
```
Authorization: AUTH_KEY
```
Please consult our team to get a valid AUTH_KEY .


## Base URL
URL of the API is ```https://secure.sparcstart.com/api/```


## Search for videos
The API endpoint to search for videos and return a paginated list is:
```
video/search
```
A complete url call looks like
```
https://secure.sparcstart.com/api/video/search?pageSize=5&pageNumber=1&filter=test&dateSortingAsc=false&companyName=demo
```
where the parameters are

* pageSize: is the max number of elements to return in each query
* pageNumber: is the current page of the elements returned
* filter: is a text that must be one of the tags or part of the name of the videos. Is used to filter the output
* companyName: the name of the company whose videos will be queried, please take in account that as part of the URL.need to be escaped if necessary.

### How to use it:
Below, an exmple of query using curl
```
 curl --request GET --url 'https://secure.sparcstart.com/api/video/search?pageSize=5&pageNumber=1&filter=test&companyName=demo' --header 'Authorization: AUTH_KEY_PROVIDEDBY_SPARC'
```

## Output
Below there is a sample of the output that we can get from the endpoint
```
{
    "ContentEncoding": null,
    "ContentType": "application/json",
    "Data": {
        "Items": [
            {
                "CompanyVideoId": 737,
                "Name": "Hidden",
                "Tags": [
                    "Test",
                    "Test6"
                ],
                "CompanyName": "Demo",
                "ViewUrl": "https://vimeopro.com/user26531028/sparcstart/video/454404865",
                "ThumbnailUrl": "https://i.vimeocdn.com/video/951357470_295x166.jpg?r=pad"
            },
            {
                "CompanyVideoId": 759,
                "Name": "Testing invitation Panel  IV",
                "Tags": [
                    "Matthew"
                ],
                "CompanyName": "Demo",
                "ViewUrl": "",
                "ThumbnailUrl": "https://i.vimeocdn.com/video/1061478233_295x166.jpg?r=pad"
            },
            {
                "CompanyVideoId": 733,
                "Name": "testing invitation Panel ",
                "Tags": [
                    "Matthew"
                ],
                "CompanyName": "Demo",
                "ViewUrl": "",
                "ThumbnailUrl": "https://i.vimeocdn.com/video/927142239_295x166.jpg?r=pad"
            },
            {
                "CompanyVideoId": 758,
                "Name": "Testing invitation Panel III",
                "Tags": [
                    "Matthew"
                ],
                "CompanyName": "Demo",
                "ViewUrl": "https://vimeopro.com/user26531028/sparcstart/video/512655025",
                "ThumbnailUrl": "https://i.vimeocdn.com/video/1061457648_295x166.jpg?r=pad"
            },
            {
                "CompanyVideoId": 757,
                "Name": "Testing invitation Panel 2",
                "Tags": [
                    "Matthew"
                ],
                "CompanyName": "Demo",
                "ViewUrl": "https://vimeopro.com/user26531028/sparcstart/video/512649638",
                "ThumbnailUrl": "https://i.vimeocdn.com/video/1061447587_295x166.jpg?r=pad"
            }
        ],
        "TotalRecords": 15
    },
    "JsonRequestBehavior": 1,
    "MaxJsonLength": null,
    "RecursionLimit": null
}
```
The TotalRecords value indicate the final number of records, so as you can see only 5 videos of 15 are obtained in this query. 
Each item will contain:

* CompanyVideoId: our internal video id
* Name: Name of the video
* Tags: tags sets to the video
* CompanyName: the company name
* ViewUrl: URL of the approved video, empty for videos not approved
* ThumbnailUrl: Url of a thumnbail image of tghe video
  

